﻿namespace ParseItTest

open ParseIt

module Helpers =
    let isSuccess (result: ParseResult<'T, 'State>) : bool =
        match result with
            | Success(_, _) -> true
            | _ -> false

    let isFailure (result : ParseResult<'T, 'State>) : bool =
        match result with
            | Failure(_, _) -> true
            | _ -> false

    let valueOf (result : ParseResult<'T, 'State>) : 'T = 
        match result with
            | Success(x, _) -> x
            | Failure(_, reason) -> failwith reason

    let stateOf (result : ParseResult<'T, 'State>) =
        match result with
            | Success(_, s) -> s
            | Failure(_, reason) -> failwith reason

    let failureReason (result : ParseResult<'T, 'State>) =
        match result with
            | Failure(_, reason) -> reason
            | _ -> failwith "Expected failure result"

    let apply parser = (parser None)

    let applyWith state parser = (parser state)

