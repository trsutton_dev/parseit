﻿namespace ParseItTest

open Xunit
open ParseIt

module CombinatorTests =
    open Helpers
    open ParseIt.Combinators

    let Any =
        fun (state: string) ->
            match (state |> Seq.toList) with
            | [] -> Failure(state, "end of input")
            | x :: xs -> Success(x, System.String.Concat(xs))

    let Character (c : char) = Any |> Combinators.Sat (fun cp -> c = cp) "character"

    module ``Return`` =
        [<Fact>]
        let ``returns a Success result of the given value without consuming state``() =
            let result = (Return 5) "state"
            Assert.Equal(5, valueOf result)
            Assert.Equal("state", stateOf result)

    module ``Fail`` =
        [<Fact>]
        let ``returns a Failure result with the given reason``() =
            Assert.Equal(Failure("state", "reason"), (Fail "reason") |> applyWith "state")

    module ``Sat`` =
        let isOdd = fun x -> x % 2 <> 0

        [<Fact>]
        let ``returns a Success result if the parsed value satisfies the given condition``() =
            Assert.Equal(Success(99, ""), (Return 99) |> Sat isOdd "an odd number" |> applyWith "")

        [<Fact>]
        let ``returns a Failure result if the given parser does not parse successfully``() =
            Assert.Equal(Failure("", "not a number (Expected an odd number)"), Fail "not a number" |> Sat isOdd "an odd number" |> applyWith "")

        [<Fact>]
        let ``returns a Failure reuslt if the parsed value does not satisfy the given condition``() =
            Assert.Equal(Failure("", "Expected an odd number"), Return 100 |> Sat isOdd "an odd number" |> applyWith "")

    module ``Where`` =
        let isOdd = fun x -> x % 2 <> 0

        [<Fact>]
        let ``returns a Success result if the parsed value satisfies the given predicate``() =
            Assert.Equal(Success(99, ""), (Return 99) |> Where isOdd |> applyWith "")

        [<Fact>]
        let ``returns a Failure result if the given parser does not parse successfully``() =
            Assert.Equal(Failure("", "not a number (Expected the item to satisfy the given predicate)"), Fail "not a number" |> Where isOdd |> applyWith "")

        [<Fact>]
        let ``returns a Failure reuslt if the parsed value does not satisfy the given predicate``() =
            Assert.Equal(Failure("", "Expected the item to satisfy the given predicate"), Return 100 |> Where isOdd  |> applyWith "")

    module ``Bind`` =
        [<Fact>]
        let ``binds the result of one parser to another``() =
            let parser = (Return 5) >>= (fun x -> Return (x * x))
            Assert.Equal(Success(25, ""), parser |> applyWith "")

        [<Fact>]
        let ``returns a failure if the parser fails``() =
            let parser = (Fail "error") >>= (fun x -> Fail "Should not get here")
            Assert.Equal(Failure("", "error"), parser |> applyWith "")

    module ``Then`` =
        [<Fact>]
        let ``applies the given parsers in succession, returning the results of the second parser``() =
            Assert.Equal(Success(4, ""), ((Return 5) |> Then (Return 4)) |> applyWith "")
            Assert.Equal(Failure("", "error 1"), ((Return 5) |> Then (Fail "error 1")) |> applyWith "")

        [<Fact>]
        let ``fails if the first parser fails``() =
            Assert.Equal(Failure("", "error 1"), (Fail "error 1") |> Then (Fail "error 2") |> applyWith "")

    module ``Or`` =
        [<Fact>]
        let ``succeeds if the first parser succeeds``() =
            Assert.Equal(Success(4, ""), (Return 4 <|> Return 5) |> applyWith "")
             
        [<Fact>]
        let ``succeeds if the second parser succeeds``() =
            Assert.Equal(Success(5, ""), (Fail "error" <|> Return 5) |> applyWith "")

        [<Fact>]
        let ``fails if both parsers fail``() =
            Assert.Equal(Failure("", "error 2"), Fail "error 1" <|> Fail "error 2" |> applyWith "")

    module ``LookAhead`` =
        [<Fact>]
        let ``looks ahead for the given value without consuming state``() =
            let parser = LookAhead (Return 3)
            let state = obj()
            Assert.Equal(Success(3, state), (LookAhead parser) |> applyWith state)

        [<Fact>]
        let ``fails if the parser fails``() =
            Assert.Equal(Failure("", "error"), LookAhead (Fail "error") |> applyWith "")

    module ``Apply`` =
        [<Fact>]
        let ``applies the given transform to the results of the parser``() =
            let parser = Return 10 |> Apply (fun n -> n * 2)
            Assert.Equal(Success(20, ""), parser |> applyWith "")

        [<Fact>]
        let ``fails if the parser fails``() =
            let parser = Fail "reason" |> Apply (fun _ -> failwith "should have failed")
            Assert.Equal(Failure("", "reason"), parser |> applyWith "")

    module ``Select`` =
        [<Fact>]
        let ``applies the given selector to the results of the parser``() =
            let parser = Return 10 |> Select (fun n -> n * 2)
            Assert.Equal(Success(20, ""), parser |> applyWith "")

        [<Fact>]
        let ``fails if the parser fails``() =
            let parser = Fail "reason" |> Select (fun _ -> failwith "should have failed")
            Assert.Equal(Failure("", "reason"), parser |> applyWith "")

    module ``SelectMany`` =
        [<Fact>]
        let ``applies each parser and projects their results to a final result``() =
            let selector = fun n -> Return (n + 1)
            let projector = fun x y -> (x, y)
            let parser = (Return 5) |> SelectMany selector projector
            Assert.Equal(Success((5, 6), ""), parser |> applyWith "")

    module ``NotFollowedBy`` =
        [<Fact>]
        let ``applies the given parser and succeeds if it is not followed by the disallowed parser``() =
            let parser = Character 'a' |> NotFollowedBy (Character 'a') "no double A's"
            Assert.Equal(Success('a', "bc"), parser |> applyWith "abc")
            Assert.True(parser |> applyWith "aabc" |> ParseResult.isFailure)

    module ``ZeroOrMore`` =
        [<Fact>]
        let ``parses zero or more instances of the given parser``() =
            let parser = ZeroOrMore (Character 'a' <|> Character 'b')
            Assert.Equal(Success([], "cabc"), parser |> applyWith "cabc")
            Assert.Equal(Success(['a'; 'b'; 'a'; 'a'], "c"), parser |> applyWith "abaac")

    module ``OneOrMore`` =
        [<Fact>]
        let ``parses one or more instances of the given parser``() =
            let parser = OneOrMore (Character 'a' <|> Character 'b')
            Assert.True(parser |> applyWith "cabc" |> ParseResult.isFailure)
            Assert.Equal(Success(['a'; 'b'; 'a'; 'a'], "c"), parser |> applyWith "abaac")

    module ``TakeUntil`` =
        [<Fact>]
        let ``takes instances of the given parser until the given delimiter is parsed``() =
            let parser = (OneOrMore (Character 'a' <|> Character 'b')) |> TakeUntil (Character 'c')
            Assert.Equal(Success([['a'; 'b'; 'a'; 'a';]], "ab"), parser |> applyWith "abaacab")
            Assert.Equal(Success([], "def"), parser |> applyWith "cdef")
            Assert.True(parser |> applyWith "def" |> ParseResult.isFailure)

    module ``Between`` =
        [<Fact>]
        let ``takes all instances of the given parser between the given delimiters``() =
            let parser = (Any |> Between (Character '<') (Character '>'))
            Assert.Equal(Success(['t'; 'e'; 's'; 't'], ""), parser |> applyWith "<test>")
            Assert.True(parser |> applyWith "test>" |> ParseResult.isFailure)
            Assert.True(parser |> applyWith "<test" |> ParseResult.isFailure)

    module ``Choice`` =
        [<Fact>]
        let ``Applies the given parsers in order returning the result of the first one that succeeds``() =
            let parser =  Choice [Character 'a'; Character 'b'; Character 'c']
            Assert.Equal(Success('b', "lack"), parser |> applyWith "black")

        [<Fact>]
        let ``Returns a failure when provided no choices``() =
            Assert.True(Choice [] |> applyWith "state" |> ParseResult.isFailure)

        [<Fact>]
        let ``Fails if none of the parsers in the list apply``() =
            let parser = Choice [Character 'a'; Character 'e'; Character 'i'; Character 'o'; Character 'u']
            Assert.True(parser |> applyWith "synth" |> ParseResult.isFailure)

    module ``Count`` =
        [<Fact>]
        let ``Counts n instances of the given parser``() =
            let parser = (Character 'a') |> Count 5
            Assert.Equal(Success(['a'; 'a'; 'a'; 'a'; 'a'], "a"), parser |> applyWith "aaaaaa")

        [<Fact>]
        let ``Fails when there is less than n instances of the given parser``() =
            let parser = (Character 'a') |> Count 5
            Assert.True(parser |> applyWith "aaaa" |> ParseResult.isFailure)

        [<Fact>]
        let ``Returns an empty list when n is less than or equal to zero``() =
            Assert.Equal(Success([], "aaaaa"), Character 'a' |> Count -1 |> applyWith "aaaaa")
            Assert.Equal(Success([], "aaaaa"), Character 'a' |> Count 0 |> applyWith "aaaaa")


    module ``Skip`` =
        [<Fact>]
        let ``Applies the given parser skipping the result``() =
            Assert.Equal(Success((), "abb"), Skip (Character 'a') |> applyWith "aabb")

        [<Fact>]
        let ``Fails if the given parser fails``() =
            Assert.True(Skip (Character 'x') |> applyWith "zyx" |> ParseResult.isFailure)

    module ``SkipMany`` =
        [<Fact>]
        let ``Applies the given parser one or more times, skipping their results``() =
            Assert.Equal(Success((), "bb"), SkipMany (Character 'a') |> applyWith "aaabb")

        [<Fact>]
        let ``Fails if the given parser cannot be applied at least once``() =
            Assert.True(SkipMany (Character 'b') |> applyWith "abc" |> ParseResult.isFailure)

    module ``Many`` =
        [<Fact>]
        let ``Applies the given parser one or more times returning the results``() =
            Assert.Equal(Success(['a'; 'a'], "bb"), Many (Character 'a') |> applyWith "aabb")

        [<Fact>]
        let ``Fails if the parser cannot be applied at least once``() =
            Assert.True(Many (Character '1') |> applyWith "000" |> ParseResult.isFailure)

    module ``SepBy1`` =
        [<Fact>]
        let ``Parses one or more occurances of a given parser separated by the given separator``() =
            let parser = Any |> SepBy1 (Character ',')
            Assert.Equal(Success(['a'], ""), parser |> applyWith "a")
            Assert.Equal(Success(['a'; 'b'; 'c';], ","), parser |> applyWith "a,b,c,")
            Assert.Equal(Success(['a'; 'b'; 'c'; 'd'], ""), parser |> applyWith "a,b,c,d")

        [<Fact>]
        let ``Fails if there are zero occurances of parser 'p'``() =
            Assert.True(Character '1' |> SepBy1 (Character ' ') |> applyWith "0 1 1" |> ParseResult.isFailure)

    module ``SepBy`` =
        let parser = Any |> Sat (System.Char.IsLetter) "letter" |> SepBy (Character ' ')
        
        [<Fact>]
        let ``returns an empty list when zero occurances of parser p``() = 
            Assert.Equal(Success([], "1 2"), parser |> applyWith "1 2")

        [<Fact>]
        let ``parses one or more occurances of a given parser separated by the given separator``() =
            Assert.Equal(Success(['a'; 'b'; 'c'], " "), parser |> applyWith "a b c ")
            Assert.Equal(Success(['t'; 'e'; 's'; 't'], ""), parser |> applyWith "t e s t")

    module ``EndBy`` =
        let parser = Any |> Sat (System.Char.IsLetter) "letter" |> EndBy (Character ';')
        
        [<Fact>]
        let ``returns an empty list when zero occurances of parser p``() = 
            Assert.Equal(Success([], "1;2;"), parser |> applyWith "1;2;")

        [<Fact>]
        let ``parses one or more occurances of a given parser separated and ended by the given separator``() =
            Assert.Equal(Success(['a'; 'b'; 'c'], "d"), parser |> applyWith "a;b;c;d")
            Assert.Equal(Success(['t'; 'e'; 's'; 't'], ""), parser |> applyWith "t;e;s;t;")

    module ``EndBy1`` =
        [<Fact>]
        let ``Parses one or more occurances of a given parser separated and ended by the given separator``() =
            let parser = Any |> EndBy1 (Character ',')
            Assert.Equal(Success(['a'], ""), parser |> applyWith "a,")
            Assert.Equal(Success(['a'; 'b'; 'c';], "d"), parser |> applyWith "a,b,c,d")
            Assert.Equal(Success(['a'; 'b'; 'c'; 'd'], ""), parser |> applyWith "a,b,c,d,")

        [<Fact>]
        let ``Fails if there are zero occurances of the parser``() =
            Assert.True(Character '1' |> EndBy1 (Character ';') |> applyWith "0;" |> ParseResult.isFailure)

        [<Fact>]
        let ``Fails if there are the parser is not followed by the separator``() =
            Assert.True(Character '1' |> EndBy1 (Character ';') |> applyWith "0" |> ParseResult.isFailure)

    module ``SepEndBy`` =
        let parser = Any |> Sat (System.Char.IsLetter) "letter" |> SepEndBy (Character ';')
        
        [<Fact>]
        let ``returns an empty list when zero occurances of parser p``() = 
            Assert.Equal(Success([], "1;2;"), parser |> applyWith "1;2;")

        [<Fact>]
        let ``parses one or more occurances of a given parser separated and optionally ended by the given separator``() =
            Assert.Equal(Success(['a'; 'b'; 'c'], "d"), parser |> applyWith "a;b;cd")
            Assert.Equal(Success(['t'; 'e'; 's'; 't'], ""), parser |> applyWith "t;e;s;t;")

    module ``SepEndBy1`` =
        [<Fact>]
        let ``Parses one or more occurances of a given parser separated and optionally ended by the given separator``() =
            let parser = Any |> SepEndBy1 (Character ',')
            Assert.Equal(Success(['a'], ""), parser |> applyWith "a,")
            Assert.Equal(Success(['a'; 'b'; 'c'; 'd';], "e"), parser |> applyWith "a,b,c,de")
            Assert.Equal(Success(['a'; 'b'; 'c'; 'd'], ""), parser |> applyWith "a,b,c,d")

        [<Fact>]
        let ``Fails if there are zero occurances of the parser``() =
            Assert.True(Character '1' |> SepEndBy1 (Character ';') |> applyWith "0" |> ParseResult.isFailure)

    module ``ChainLeft`` =
        let parser = 
            Any
            |> Sat (System.Char.IsDigit) "digit"
            |> Apply (fun c -> System.Int32.Parse(c.ToString())) 
            |> ChainLeft (Character '*' |> Then (Return (*))) (Return 0)

        [<Fact>]
        let ``Returns the default value when there are zero instances of the given parser``() =
            Assert.Equal(Success(0, "a*b"), parser |> applyWith "a*b")

        [<Fact>]
        let ``Parses one or more instances of the given parser separated by the given operation``() =
            Assert.Equal(Success(6, ""), parser |> applyWith "2*3")
            Assert.Equal(Success(2, "+3"), parser |> applyWith "2+3")

    module ``ChainLeft1`` =
        let parser =
            Any
            |> Sat (System.Char.IsDigit) "digit"
            |> Apply (fun c -> System.Int32.Parse(c.ToString()))
            |> ChainLeft1 
                ((Character '*' |> Then (Return (*))) 
                <|> (Character '/' |> Then (Return (/)))
                <|> (Character '+' |> Then (Return (+)))
                <|> (Character '-' |> Then (Return (-))))

        [<Fact>]
        let ``Parses one or more instances of the given parser separated by the given operation``() =
            Assert.Equal(Success(6, " = 6"), parser |> applyWith "1+2*3*4/6 = 6")
            Assert.Equal(Success(1, "%2"), parser |> applyWith "1%2")

        [<Fact>]
        let ``Fails if one instance of the parser separated by the operation cannot be parsed``() =
            Assert.True(parser |> applyWith "a+2" |> ParseResult.isFailure)

    module ``ChainRight`` =
        let parser = 
            Any
            |> Sat (System.Char.IsDigit) "digit"
            |> Apply (fun c -> System.Int32.Parse(c.ToString())) 
            |> ChainRight (Character '*' |> Then (Return (*))) (Return 0)

        [<Fact>]
        let ``Returns the default value when there are zero instances of the given parser``() =
            Assert.Equal(Success(0, "a*b"), parser |> applyWith "a*b")

        [<Fact>]
        let ``Parses one or more instances of the given parser separated by the given operation``() =
            Assert.Equal(Success(6, ""), parser |> applyWith "2*3")
            Assert.Equal(Success(2, "+3"), parser |> applyWith "2+3")

    module ``ChainRight1`` =
        let parser =
            Any
            |> Sat (System.Char.IsDigit) "digit"
            |> Apply (fun c -> System.Int32.Parse(c.ToString()))
            |> ChainRight1 
                ((Character '*' |> Then (Return (*))) 
                <|> (Character '/' |> Then (Return (/)))
                <|> (Character '+' |> Then (Return (+)))
                <|> (Character '-' |> Then (Return (-))))

        [<Fact>]
        let ``Parses one or more instances of the given parser separated by the given operation using a right associative application``() =
            Assert.Equal(Success(7, " = 7"), parser |> applyWith "1+2*3*6/6 = 7")
            Assert.Equal(Success(1, "%2"), parser |> applyWith "1%2")

        [<Fact>]
        let ``Fails if one instance of the parser separated by the operation cannot be parsed``() =
            Assert.True(parser |> applyWith "a+2" |> ParseResult.isFailure)
