﻿namespace ParseItTest

open Xunit
open ParseIt

module CharParserTests =
    open System
    open Helpers
    open ParseIt.Combinators
    open ParseIt.Parsers

    let parse (text : System.String) parser =
        let state = TextParserState(ParserPosition(0, 0), Text(text.Split('\n')))
        (parser state)

    let stateString (result : ParseResult<'T, TextParserState>) =
        (stateOf result).ParserStateToString()

    let tp = TextParser()

    module ``Any`` =
        [<Fact>]
        let ``Successfully parses any character, updating the state``() =
            let result = tp.Any |> parse "abc"
            Assert.True(result |> isSuccess)
            Assert.Equal('a', valueOf result)
            Assert.Equal("a^bc", result |> stateString)

        [<Fact>]
        let ``Fails if at the end of input``() =
            let result = tp.Any |> parse ""
            Assert.True(result |> isFailure)
            Assert.Equal("unexpected end of input", result |> failureReason)


    module ``Character`` =
        [<Fact>]
        let ``Succeeds if the given character is the next character in the input stream``() =
            let result = tp.Character 'a' |> parse "abc"
            Assert.True(result |> isSuccess)
            Assert.Equal('a', valueOf result)
            Assert.Equal("a^bc", result |> stateString)

        [<Fact>]
        let ``Fails if the next character in the input stream does not match the given character``() =
            let result = tp.Character 'a' |> parse "Abc"
            Assert.True(result |> isFailure)
            Assert.Equal("Expected character", result |> failureReason)

    
    module ``CharacterIgnoringCase`` =
        [<Fact>]
        let ``Succeeds if the given character is the next character in the input stream, ignoring case``() =
            let result = tp.CharacterIgnoringCase 'a' |> parse "Abc"
            Assert.True(result |> isSuccess)
            Assert.Equal('A', valueOf result)
            Assert.Equal("A^bc", result |> stateString)
             
        [<Fact>]
        let ``Fails if the next character does not match the given character``() =
            let result = tp.CharacterIgnoringCase 'A' |> parse "def"
            Assert.True(result |> isFailure)
            Assert.Equal("Expected character (case-insensitive)", result |> failureReason)


    module ``Letter`` =
        [<Fact>]
        let ``Parses any alpha character``() =
            let result = tp.Letter |> parse "a1"
            Assert.True(result |> isSuccess)
            Assert.Equal('a', valueOf result)
            Assert.Equal("a^1", result |> stateString)
     
        [<Fact>]
        let ``Fails if character is not an alpha character``() =
            let result = tp.Letter |> parse "1a"
            Assert.True(result |> isFailure)
            Assert.Equal("Expected letter", result |> failureReason)


    module ``Digit`` =
        [<Fact>]
        let ``Parses any numeric digit``() =
            let result = tp.Digit |> parse "123"
            Assert.True(result |> isSuccess)
            Assert.Equal('1', valueOf result)
            Assert.Equal("1^23", result |> stateString)
     
        [<Fact>]
        let ``Fails if character is not a numeric digit``() =
            let result = tp.Digit |> parse "abc"
            Assert.True(result |> isFailure)
            Assert.Equal("Expected digit", result |> failureReason)


    module ``LetterOrDigit`` =
        [<Fact>]
        let ``Parses any alpha-numeric character``() =
            let result = (OneOrMore tp.LetterOrDigit) |> parse "a1*"
            Assert.True(result |> isSuccess)
            Assert.Equal<char list>(['a'; '1'], valueOf result)
            Assert.Equal("a1^*", result |> stateString)
     
        [<Fact>]
        let ``Fails if character is not an alpha-numeric character``() =
            let result = tp.LetterOrDigit |> parse "***"
            Assert.True(result |> isFailure)
            Assert.Equal("Expected letter or digit", result |> failureReason)


    module ``LowercaseLetter`` =
        [<Fact>]
        let ``Parses any lowercase letter``() =
            let result = tp.LowercaseLetter |> parse "aAbB"
            Assert.True(result |> isSuccess)
            Assert.Equal('a', valueOf result)
            Assert.Equal("a^AbB", result |> stateString)
     
        [<Fact>]
        let ``Fails if character is not a lowercase letter``() =
            let result = tp.LowercaseLetter |> parse "AaBb"
            Assert.True(result |> isFailure)
            Assert.Equal("Expected lowercase letter", result |> failureReason)


    module ``UppercaseLetter`` =
        [<Fact>]
        let ``Parses any uppercase letter``() =
            let result = tp.UppercaseLetter |> parse "AaBb"
            Assert.True(result |> isSuccess)
            Assert.Equal('A', valueOf result)
            Assert.Equal("A^aBb", result |> stateString)
     
        [<Fact>]
        let ``Fails if character is not a uppercase letter``() =
            let result = tp.UppercaseLetter |> parse "aAbB"
            Assert.True(result |> isFailure)
            Assert.Equal("Expected uppercase letter", result |> failureReason)


    module ``Literal`` =
        [<Fact>]
        let ``Parses the given string``() =
            let result = tp.Literal "This is a test" |> parse "This is a test!!"
            Assert.True(result |> isSuccess)
            Assert.Equal("This is a test", valueOf result)
            Assert.Equal("This is a test^!!", result |> stateString)
     
        [<Fact>]
        let ``Fails if it cannot parse the given string``() =
            let result = tp.Literal "test" |> parse "oops"
            Assert.True(result |> isFailure)
            Assert.Equal("Failed to parse string literal \"test\"", result |> failureReason)
