# ParserIt
ParseIt is parser combinator library designed to help simplify writing parsers.

A parser is defined as:
```
type Parser<'T, 'State> = 'State -> ParseResult<'T, 'State>
```

A parser is simply a function that takes a generic state as an input and returns a
ParseResult that represents a Success or Failure. 

* A ***Success*** result provides the parsed value and the state after parsing 
* A ***Failure*** result provides the state of the parser at the time of the error and the
reason for the failure

```
type ParseResult<'T, 'State> =
    | Success of 'T * 'State
    | Failure of 'State * string
```
  

## Parser Combinators
The [ParseIt.Combinators](#) module comes with a number of generic parser combinators that can be used to
create and combine parsers in a way that allows one to easily define more complex parsers. The module
also defines some custom operators to allow for a more terse syntax.

* Return
* Fail
* Sat 
* Where
* Bind (`>>=`) 
* Then
* Or (`<|>`)
* Match
* LookAhead
* Apply 
* Select 
* SelectMany
* NotFollowedBy
* ZeroOrMore 
* OneOrMore 
* Many
* Optional
* TakeUntil 
* ManyTill
* Between
* Choice
* Count
* Skip 
* SkipMany
* SepBy 
* SepBy1 
* EndBy 
* EndBy1 
* SepEndBy 
* SepEndBy1
* ChainLeft 
* ChainLeft1 
* ChainRight 
* ChainRight1

The library also defines a builder class (computation expression builder) to assist with constructing examples.
_See the example below for more details and don't forget to check out the [documentation](#))_.

## Text Parsers
In addition to the combinators, the library also defines some parsers that lend themselves to 
parsing text (See [TextParser](#)).

* Any
* Character
* CharacterIgnoringCase
* Letter
* Digit
* LetterOrDigit
* LowercaseLetter
* UppercaseLetter
* Literal

<br>

## Parser Example
Okay! Let's dive in with some concrete examples:

Let's start with a simple grammar for an expression in BNF format:

```
<expr> ::= <term> "+" <expr>
         |  <term>

<term> ::= <factor> "*" <term>
         |  <factor>

<factor> ::= "(" <expr> ")"
           |  <const>

<const> ::= integer  
```

Here we're going to use a [TextParser](#) and a [ParserBuilder](#) to help construct the parser:

```
module ``Expression Grammar Example`` =
    open ParseIt
    open ParseIt.Combinators
    open ParseIt.Parsers

    let tp = TextParser()
    let build = ParserBuilder()

    let rec expr() =
        build {
            let! t = term()
            let! _ = tp.Character '+'
            let! e = expr()
            return sprintf "%O + %O" t e 
        }
        <|>
        term()

    and term() =
        build {
            let! f = factor()
            let! _ = tp.Character '*'
            let! t = term()
            return sprintf "%O * %O" f t
        }
        <|>
        factor()

    and factor() =
        build {
            let! lp = tp.Character '('
            let! e = expr()
            let! rp = tp.Character ')'
            return sprintf "( %O )" e 
        }
        <|>
        constInt()

    and constInt() = integer()

    and integer() =
        build {
            let! s = Optional (sign())
            let! digits = OneOrMore tp.Digit
            let sgn = if s.IsSome then s.Value else "" 
            return sprintf "%O%O" sgn (System.String.Concat(digits))
        }

    and sign() = tp.Literal "+" <|> tp.Literal "-"

    let parse (text : System.String) =
        let state = TextParserState(ParserPosition(0, 0), Text(text.Split('\n')))
        (expr() state)
```