﻿namespace ParseIt.Parsers

open System
open System.IO
open System.Text

open ParseIt
open ParseIt.Combinators

type ParserPosition(line: int, column : int) =
    member this.Line with get() = line
    member this.Column with get() = column
    override this.ToString() = sprintf "(%i, %i)" this.Line this.Column

type TextInput =
    | Text of System.String array

type Input =
    | TextInput of TextInput

[<AbstractClass>]
type ParserState<'T>(position: ParserPosition, input: Input) =
    member this.Position with get() = position
    member this.Input with get() = input
    abstract member ParserStateToString : unit -> System.String
    abstract member GetLine : int * ParserPosition -> System.String
    override this.ToString() = this.ParserStateToString()
         
type TextParserState(position: ParserPosition, input: TextInput) =
    inherit ParserState<char>(position, TextInput(input))

    override this.GetLine (lineno : int, pos : ParserPosition) =
        match input with
        | Text(lines) -> 
            if lineno < 0 || lineno >= lines.Length then
                failwith "invalid index"
            let line = lines.[lineno]
            if pos.Line = lineno then line.Insert(pos.Column, "^")
            else line

    override this.ParserStateToString() =
        match input with
        | Text(lines) ->
            if lines.Length = 0 then "^"
            else
                let mutable (str : System.String) = if this.Position.Line = 0 && this.Position.Column = 0 then "^" else ""
                for lineno in 0 .. (lines.Length - 1) do
                    str <- str + this.GetLine(lineno, this.Position)
                str

type ParserBuilder() =
    member x.Bind (parser, binder) = parser >>= binder
    member x.Return value = Return value

[<AbstractClass>]
type AbstractParser<'T, 'State>() =
    abstract member Any : Parser<'T, 'State>
    