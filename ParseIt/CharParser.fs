﻿namespace ParseIt.Parsers

open ParseIt
open ParseIt.Combinators
open ParseIt.Parsers

[<AbstractClass>]
type CharParser<'State>() =
    inherit AbstractParser<char, 'State>()

    member this.Character c = 
        this.Any |> Sat (fun cp -> cp = c) "character"


    member this.CharacterIgnoringCase c =
        let comparer = fun character -> System.String.Equals(character.ToString(), c.ToString(), System.StringComparison.InvariantCultureIgnoreCase)
        this.Any |> Sat comparer "character (case-insensitive)"

    
    member this.Letter = this.Any |> Sat System.Char.IsLetter "letter"


    member this.Digit = this.Any |> Sat System.Char.IsDigit "digit"


    member this.LetterOrDigit = this.Any |> Sat System.Char.IsLetterOrDigit "letter or digit"

    
    member this.LowercaseLetter = this.Any |> Sat System.Char.IsLower "lowercase letter"


    member this.UppercaseLetter = this.Any |> Sat System.Char.IsUpper "uppercase letter"


    member this.Literal literal =
        let rec helper str rest : Parser<string, 'State>  =
            fun state ->
                if System.String.IsNullOrEmpty(rest) then Success(str, state)
                else (this.Character rest.[0] >>= (fun _ -> (helper str (rest.Substring(1)))))(state)
        fun state ->
            match (helper literal literal) state with
            | Success(value, newState) -> Success(value, newState)
            | Failure(_, _) -> Failure(state, sprintf "Failed to parse string literal \"%s\"" literal)


type TextParser() =
    inherit CharParser<TextParserState>()

    override this.Any =  
        fun (state : TextParserState) ->
            match state.Input with
                | Input.TextInput(Text(lines)) ->
                    let isEndOfInput = (state.Position.Line + 1) >= lines.Length && state.Position.Column >= lines.[state.Position.Line].Length
                    if isEndOfInput then Failure(state, "unexpected end of input")
                    else
                        let line = lines.[state.Position.Line]
                        let newPosition = 
                            if state.Position.Column >= (line.Length - 1) then ParserPosition(state.Position.Line + 1, 0) 
                            else ParserPosition(state.Position.Line, state.Position.Column + 1)
                        Success(line.[state.Position.Column], TextParserState(newPosition, Text(lines)))