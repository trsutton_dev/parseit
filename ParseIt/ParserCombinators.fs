﻿namespace ParseIt

type ParseResult<'T, 'State> =
    | Success of 'T * 'State
    | Failure of 'State * string

type Parser<'T, 'State> = 'State -> ParseResult<'T, 'State>

module ParseResult =
    let isSuccess = function
        | Success(_, _) -> true
        | _ -> false

    let isFailure = function
        | Failure(_,_) -> true
        | _ -> false

module Combinators =
    let Return<'T, 'State> (value : 'T) : Parser<'T, 'State> =
        fun state -> Success(value, state)


    let Fail<'T, 'State> reason : Parser<'T, 'State> =
        fun state -> Failure(state, reason)


    let Sat<'T, 'State> (condition : 'T -> bool) tag (parser : Parser<'T, 'State>) : Parser<'T, 'State> =
        fun state ->
            match parser state with
                | Success(value, newState) when condition value -> Success(value, newState)
                | Success(_, _)                                 -> Failure(state, sprintf "Expected %O" tag)
                | Failure(_, reason)                            -> Failure(state, sprintf "%O (Expected %O)" reason tag)


    let Where<'T, 'State> predicate parser : Parser<'T, 'State> =
        parser |> Sat predicate "the item to satisfy the given predicate"
         

    let Bind<'T, 'U, 'State> (binder : 'T -> Parser<'U, 'State>) (parser : Parser<'T, 'State>) : Parser<'U, 'State> =
        fun state ->
            match parser state with
                | Success(value, newState)  -> (binder value)(newState)
                | Failure(failureState, error) -> Failure(failureState, error)


    let inline (>>=) (parser : Parser<'T, 'State>) binder : Parser<'U, 'State> =
        parser |> Bind binder


    let Then<'T, 'U, 'State> (other : Parser<'U, 'State>) (parser : Parser<'T, 'State>) : Parser<'U, 'State> =
        parser >>= (fun _ -> other)


    let Or<'T, 'State> (other : Parser<'T, 'State>) (parser : Parser<'T, 'State>) : Parser<'T, 'State> =
        fun state ->
            match parser state with
                | Success(_, _) as success  -> success
                | Failure(_, _)             -> other state


    let inline (<|>) parser other : Parser<'T, 'State> =
        parser |> Or other


    let Match onSuccess onFailure (parser: Parser<'T, 'State>) : Parser<'T, 'State> =
        fun state ->
            match parser state with
                | Success(value, newState) -> onSuccess(value, newState)
                | Failure(failureState, reason) -> onFailure(failureState, reason)

    let LookAhead parser : Parser<'T, 'State> =
        fun state ->
            match parser state with
                | Success(value, _) -> Success(value, state)
                | Failure(failureState, reason)   -> Failure(failureState, reason)
        
            
    let Apply<'T, 'U, 'State> (transform : 'T -> 'U) (parser : Parser<'T, 'State>) : Parser<'U, 'State> =
        fun state ->
            match parser state with
                | Success(value, newState)      -> Success(transform value, newState)
                | Failure(failureState, reason) -> Failure(failureState, reason)


    let Select<'T, 'U, 'State> (selector : 'T -> 'U) parser : Parser<'U, 'State> =
        parser |> Apply selector


    let SelectMany<'T, 'U, 'V, 'State> (selector : 'T -> Parser<'U, 'State>) (projector : 'T -> 'U -> 'V) (parser : Parser<'T, 'State>) : Parser<'V, 'State> =
        parser >>= (fun t -> (selector t) >>= (fun u -> (Return (projector t u))))


    let NotFollowedBy<'T, 'U, 'State> (disallowed : Parser<'U, 'State>) tag (parser : Parser<'T, 'State>) : Parser<'T, 'State> =
        fun state ->
            match parser state with
                | Failure(failureState, reason) -> Failure(failureState, reason)
                | Success(value, newState) ->
                    match disallowed newState with
                        | Success(_, _) -> Failure(state, tag)
                        | Failure(_, _) -> Success(value, newState)


    let rec ZeroOrMore (parser : Parser<'T, 'State>) : Parser<'T list, 'State> =
        (OneOrMore parser) <|> Return([])


    and OneOrMore (parser : Parser<'T, 'State>) : Parser<'T list, 'State> =
        parser >>= (fun t -> (ZeroOrMore parser) >>= (fun ts -> Return(t :: ts)))


    let Many (parser : Parser<'T, 'State>) : Parser<'T list, 'State> =
        OneOrMore parser


    let Optional<'T, 'State> (parser : Parser<'T, 'State>) : Parser<'T option, 'State> =
        fun state ->
            match parser state with
                | Success(value, newState)  -> Success(Some value, newState)
                | Failure(_, _)             -> Success(None, state)


    let TakeUntil<'T, 'U, 'State> (delimiter : Parser<'U, 'State>) (parser : Parser<'T, 'State>) : Parser<'T list, 'State> =
        let rec helper p d ts =
            let binder = fun x ->
                match x with
                    | Some(_) -> Return (List.rev ts)
                    | None    -> p >>= (fun t -> helper p d (t :: ts))
            (Optional d) >>= binder
        helper parser delimiter []
        
    
    let ManyTill<'T, 'U, 'State> (delimiter : Parser<'U, 'State>) (parser : Parser<'T, 'State>) : Parser<'T list, 'State> =
        parser |> TakeUntil delimiter 


    let Between<'T, 'U, 'V, 'State> (startDelimiter : Parser<'U, 'State>) (endDelimiter : Parser<'V, 'State>) parser : Parser<'T list, 'State> =
        startDelimiter |> Then (parser |> TakeUntil endDelimiter)


    let Choice<'T, 'State> (parsers : Parser<'T, 'State> list) : Parser<'T, 'State> =
        match parsers with
            | [] -> Fail "None of the choices could be parsed"
            | parser :: rest -> rest |> List.fold (fun acc p -> acc <|> p) parser


    let Count<'T, 'State> n (parser : Parser<'T, 'State>) : Parser<'T list, 'State> =
        if n <= 0 then Return []
        else
            let rec helper p i n acc =
                if i = n then Return (List.rev acc)
                else
                    let binder = fun x ->
                        match x with
                            | None -> Fail (sprintf "Could not parse %i instances" n)
                            | Some(t) -> helper p (i + 1) n (t :: acc)
                    (Optional p) >>= binder
            helper parser 0 n []
            
            
    let Skip<'T, 'State> (parser : Parser<'T, 'State>)  : Parser<unit, 'State> =
        parser |> Then (Return ())


    let SkipMany<'T, 'State> (parser : Parser<'T, 'State>) : Parser<unit, 'State> =
        (OneOrMore parser) |> Then (Return ())
    
    let rec SepBy (sep : Parser<'Sep, 'State>) (parser : Parser<'T, 'State>) : Parser<'T list, 'State> =
        (parser |> SepBy1 sep) <|> Return([])

    and SepBy1 (sep : Parser<'Sep, 'State>) (parser : Parser<'T, 'State>) : Parser<'T list, 'State> =
        parser |> Bind (fun t -> ZeroOrMore(sep |> Then parser) >>= (fun ts -> Return (t :: ts)))

        
    let EndBy (sep : Parser<'Sep, 'State>) (parser : Parser<'T, 'State>) : Parser<'T list, 'State> =
        ZeroOrMore (parser >>= (fun t -> sep |> Then (Return t)))
        
    
    let EndBy1 (sep : Parser<'Sep, 'State>) (parser : Parser<'T, 'State>) : Parser<'T list, 'State> =
        OneOrMore (parser >>= (fun t -> sep |> Then (Return t)))     

            
    let SepEndBy (sep : Parser<'Sep, 'State>) (parser : Parser<'T, 'State>) : Parser<'T list, 'State> =
        parser
        |> SepBy sep
        |> Bind (fun ts -> (Optional sep) |> Then (Return ts))


    let SepEndBy1 (sep : Parser<'Sep, 'State>) (parser : Parser<'T, 'State>) : Parser<'T list, 'State> =
        parser 
        |> SepBy1 sep 
        |> Bind (fun ts -> (Optional sep) |> Then (Return ts))


    let rec ChainLeft op defaultValue parser : Parser<'T, 'State> =
        (parser |> ChainLeft1 op) <|> defaultValue

    and ChainLeft1 op parser =
        let rec rest x = (op >>= (fun f -> parser >>= (fun y -> rest (f x y)))) <|> (Return x)
        parser >>= (fun t -> rest t)
        

    let rec ChainRight op x parser : Parser<'T, 'State> =
        (parser |> ChainRight1 op) <|> x
        
    and ChainRight1 op parser : Parser<'T, 'State> =
        let rec scan() = parser >>= (fun x -> rest x)
        and rest x = (op >>= (fun f -> scan() >>= (fun y -> rest (f x y)))) <|> (Return x)
        scan()
        
        
        
        
        